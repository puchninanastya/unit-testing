const server = require('../server.js');
const config = require('../config/index.js');
const fs = require('fs-extra');
const assert = require('assert');
const request = require('request');
const path = require("path");
const { COPYFILE_EXCL } = fs.constants;

describe('GET request', () => {
	let app;
	before((done) => {
		app = server.listen(3333, () => {
			done();
		});
	});

	it('get запрос к http://localhost:3333  => вернет index.html', (done) => {
		request('http://localhost:3333/', (err, res, body) => {
			const fileData = fs.readFileSync(path.resolve(__dirname, '..', config.publicRoot, 'index.html'), 'utf8');	
			assert.equal(fileData, body);
			done();
		});
	});

	it('get запрос к http://localhost:3333/file.txt => вернет file.txt', (done) => {
		const fileData = "Hello from Anastasia Puchnina ヽ(´▽｀)ノ";
		fs.writeFileSync(path.resolve(__dirname, '..', config.filesRoot, 'file.txt'), fileData);
		
		request.get('http://localhost:3333/file.txt', (err, res, body) => {
			assert.equal(fileData, body);
			fs.removeSync(path.resolve(__dirname, '..', config.filesRoot, 'file.txt'));
			done();
		});
	});
	
	after((done) => {
		app.close(() => {
		done();
		});
	});

});
